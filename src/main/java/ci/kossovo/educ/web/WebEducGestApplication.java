package ci.kossovo.educ.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebEducGestApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebEducGestApplication.class, args);
	}
}
