package ci.kossovo.educ.web.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.web.entity.Cours;

public interface CourRepository extends JpaRepository<Cours, Long> {

}
