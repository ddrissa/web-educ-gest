package ci.kossovo.educ.web.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.web.entity.EtudiantCours;
import ci.kossovo.educ.web.entity.EtudiantCoursID;

public interface EtudiantCourRepository extends JpaRepository<EtudiantCours, EtudiantCoursID> {

}
