package ci.kossovo.educ.web.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.web.entity.EtudiantEvalu;
import ci.kossovo.educ.web.entity.EtudiantEvaluID;

public interface EtudiantEvaluRepository extends JpaRepository<EtudiantEvalu, EtudiantEvaluID> {

}
