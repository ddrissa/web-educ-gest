package ci.kossovo.educ.web.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ci.kossovo.educ.web.entity.Matiere;
import java.lang.String;
import java.util.List;

public interface MatiereRepository extends JpaRepository<Matiere, Long> {
	
	// Trouver une matière par libellé.
	List<Matiere> findByLibelle(String libelle);
	
	// Trouver une matière par mot clé.
	@Query("select m from Matiere m where m.libelle like %:mc%")
	List<Matiere> rechParMotCle(@Param("mc") String mc);

}
