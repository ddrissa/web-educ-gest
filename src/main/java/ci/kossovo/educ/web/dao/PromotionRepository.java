package ci.kossovo.educ.web.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.web.entity.Promotion;

public interface PromotionRepository extends JpaRepository<Promotion, Long> {

}
