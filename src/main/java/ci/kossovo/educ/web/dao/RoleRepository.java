package ci.kossovo.educ.web.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.web.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
	List<Role> findByRoleName(String rolename);

}
