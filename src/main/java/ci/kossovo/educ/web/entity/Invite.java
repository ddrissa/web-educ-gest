package ci.kossovo.educ.web.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="T_INVITE")
@DiscriminatorValue("IN")
public class Invite extends Personne{
	private static final long serialVersionUID = 1L;
	
	private String raison;
	private String profil;
	private String societe;
	private String status;
	public Invite() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	public Invite(String titre, String nom, String prenom, String numCni) {
		super(titre, nom, prenom, numCni);
		// TODO Auto-generated constructor stub
	}



	public Invite(String titre, String nom, String prenom, String numCni, String raison, String profil, String societe,
			String status) {
		super(titre, nom, prenom, numCni);
		this.raison = raison;
		this.profil = profil;
		this.societe = societe;
		this.status = status;
	}
	public String getRaison() {
		return raison;
	}
	public void setRaison(String raison) {
		this.raison = raison;
	}
	public String getProfil() {
		return profil;
	}
	public void setProfil(String profil) {
		this.profil = profil;
	}
	public String getSociete() {
		return societe;
	}
	public void setSociete(String societe) {
		this.societe = societe;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	

}
