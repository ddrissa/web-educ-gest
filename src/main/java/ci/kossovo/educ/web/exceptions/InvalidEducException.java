package ci.kossovo.educ.web.exceptions;

public class InvalidEducException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidEducException(String message) {
		super(message);
	}

}
