package ci.kossovo.educ.web.metier;

import ci.kossovo.educ.web.entity.Cours;
import ci.kossovo.educ.web.entity.EtudiantCours;

public interface ICourMetier extends IMetier<Cours, Long> {
	
	public EtudiantCours absence(Long idCour , Long idEtudiant, boolean absence, String motif);

}
