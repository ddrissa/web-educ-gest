package ci.kossovo.educ.web.metier;

import java.util.List;

import ci.kossovo.educ.web.entity.Matiere;

public interface IMatiereMetier extends IMetier<Matiere, Long> {
	public List<Matiere> findByLibelle(String lib) ;
	public List<Matiere> rechParMotCle(String mc) ;

}
