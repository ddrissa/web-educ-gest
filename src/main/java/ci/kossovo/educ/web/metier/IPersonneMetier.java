package ci.kossovo.educ.web.metier;

import java.util.List;

import ci.kossovo.educ.web.entity.Enseigne;
import ci.kossovo.educ.web.entity.Personne;

public interface IPersonneMetier extends IMetier<Personne, Long> {
	
	public Personne chercherParMatricule(String matricule);
	public Personne chercherParIdentifiantS(String numCni);
	public List<Personne> chercherParStatus(String status);
	public List<Personne> chercherParFonction(String fonction);
	public List<Personne> chercherEtudiantParMc(String mc);
	public List<Personne> chercherEnseignantParMc(String mc);
	public List<Personne> chercherAdministrateurParMc(String mc);
	public List<Personne> chercherUserParMc(String mc);
	
	public List<Personne> personneAll(String type);
	
	public Enseigne affecterPromgMat(Long idEnseignant, Long idPromo,Long idMatiere);
	

}
