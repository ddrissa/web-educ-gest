package ci.kossovo.educ.web.metier;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ci.kossovo.educ.web.dao.PersonneRepository;
import ci.kossovo.educ.web.entity.Enseigne;
import ci.kossovo.educ.web.entity.Personne;
import ci.kossovo.educ.web.exceptions.InvalidEducException;
import ci.kossovo.educ.web.exceptions.InvalidPersonneException;


@Service
public class PersonneMetierImpl implements IPersonneMetier {

	@Autowired
	private PersonneRepository personneRepository;
	

	@Override
	@Transactional
	public Personne creer(Personne entity) throws InvalidEducException {
		if (entity.getNom() == null ||entity.getNom()=="" || entity.getPrenom() == null ||entity.getPrenom()==""
				|| entity.getNumCni() == null || entity.getNumCni()=="") {
			throw new InvalidEducException("Le nom, prenom ou numCni ne peut etre null");
		};

		Personne p=null;
		try {
			p = personneRepository.findByNumCni(entity.getNumCni());
		} catch (Exception e1) {
			throw new InvalidEducException("Probleme connexion bd.");
		}
		
		if (p != null){
			throw new InvalidEducException("Cette personne existe dejà.");
		}

		return personneRepository.save(entity);
	}

	@Override
	@Transactional
	public Personne modifier(Personne entity) throws InvalidEducException {
		Personne p = personneRepository.findByNumCni(entity.getNumCni());
		if (p!=null && entity.getId()!= p.getId()) {
			
				throw new InvalidEducException("Cet indentifiant cni existe dejà.");
		}

		return personneRepository.save(entity);
	}

	@Override

	public Personne find(Long id) {
		return personneRepository.findOne(id);
	}

	@Override
	public List<Personne> findAll() {
		return personneRepository.findAll();
	}
	
	//liste de personnes par type
	@Override
	public List<Personne> personneAll(String type) {
		List<Personne> personnes=personneRepository.findAll();
		//filtre par type de personnes
		List<Personne> typePersonnes=personnes.stream().filter(
				p-> p.getType().equals(type)).collect(Collectors.toList());
		return typePersonnes;
	}

	@Override
	@Transactional
	public void spprimer(List<Personne> entities) {
		personneRepository.delete(entities);

	}

	@Override
	@Transactional
	public boolean supprimer(Long id) {
		personneRepository.delete(id);
		return true;
	}

	@Override
	public boolean existe(Long id) {
		return personneRepository.exists(id);
	}

	@Override
	public Long compter() {
		return personneRepository.count();
	}

	@Override
	public Personne chercherParMatricule(String matricule) {
		return personneRepository.getMatriculeIgnoreCase(matricule);
	}

	@Override
	public List<Personne> chercherParStatus(String status) {
		return personneRepository.getStatusIgnoreCase(status);
	}

	@Override
	public List<Personne> chercherEtudiantParMc(String mc) {
		return personneRepository.findAllEtudiantParMc(mc);
	}

	@Override
	public List<Personne> chercherEnseignantParMc(String mc) {
		return personneRepository.findAllEnseignantParMc(mc);
	}

	@Override
	public List<Personne> chercherAdministrateurParMc(String mc) {
		return personneRepository.findAllAdministrateurParMc(mc);
	}

	@Override
	public List<Personne> chercherUserParMc(String mc) {
		return personneRepository.findByNomCompletContainingIgnoreCase(mc);
	}

	@Override
	public Personne chercherParIdentifiantS(String numCni) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Personne> chercherParFonction(String fonction) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enseigne affecterPromgMat(Long idEnseignant, Long idPromo, Long idMatiere) {
		// TODO Auto-generated method stub
		return null;
	}

	

	

	
	

}
