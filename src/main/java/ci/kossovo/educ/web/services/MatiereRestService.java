package ci.kossovo.educ.web.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ci.kossovo.educ.web.entity.Matiere;
import ci.kossovo.educ.web.exceptions.InvalidEducException;
import ci.kossovo.educ.web.metier.IMatiereMetier;
import ci.kossovo.educ.web.models.Reponse;
import ci.kossovo.educ.web.utilitaires.Static;

@CrossOrigin(origins={"http://localhost:4200", "http://localhost:4300"})
@RestController
public class MatiereRestService {
	@Autowired
	private IMatiereMetier matiereMetier;
	
	@Autowired
	private ObjectMapper jsonMapper;
	
	
	// Methode local
	private Reponse<Matiere> getMatiere(Long id) {
		// On recupère la personne
		Matiere mat = null;
		try {
			mat = matiereMetier.find(id);
		} catch (RuntimeException e) {
			return new Reponse<>(1, Static.getErreurforexception(e), null);
		}

		// personne existe?
		if (mat == null) {
			List<String> messages = new ArrayList<>();
			messages.add(String.format("La matière [%s] n'existe pas", id));
			return new Reponse<Matiere>(2, messages, null);
		}
		// OK
		return new Reponse<Matiere>(0, null, mat);
	}

	////////////////////////////////////////////////////////////////////////////////////////////

// ajouter une matiere
	@PostMapping("/matieres")
	public String creer(@RequestBody Matiere m) throws JsonProcessingException  {
		Reponse<Matiere> reponse;
		try {
			Matiere matiere=matiereMetier.creer(m);
			
			List<String> messages = new ArrayList<>();
			messages.add(String.format("[%s] [%s] a été creer avec succès!", matiere.getId(),matiere.getLibelle()));
			reponse = new Reponse<Matiere>(0, messages, matiere);
		} catch (InvalidEducException e) {
			reponse = new Reponse<Matiere>(1, Static.getErreurforexception(e), null);
		}
		return jsonMapper.writeValueAsString(reponse);
	}

	
	// modifier une matiere
	
	@PutMapping("/matieres")
	public String modifier(@RequestBody Matiere m) throws JsonProcessingException{
		Reponse<Matiere> reponse;
		Reponse<Matiere> reponseModif = getMatiere(m.getId());
		if (reponseModif.getStatus() == 0) {
			try {
				Matiere matiere=matiereMetier.modifier(m);	
				List<String> messages = new ArrayList<>();
				messages.add(String.format("[%s] [%s] a été modifier avec succès!", matiere.getId(),matiere.getLibelle()));
				reponse = new Reponse<Matiere>(0, messages,matiere );
				
			} catch (InvalidEducException e) {
				reponse = new Reponse<Matiere>(1, Static.getErreurforexception(e), null);
			}
		} else {
			reponse = new Reponse<Matiere>(reponseModif.getStatus(), reponseModif.getMessages(),
					reponseModif.getBody());
		}

		return jsonMapper.writeValueAsString(reponse);
	}

	
// trouver une matiere par son id
	
	@GetMapping("/matieres/{id}")
	public String find(@PathVariable("id") Long id) throws JsonProcessingException {
		Reponse<Matiere> reponse;
		reponse = getMatiere(id);
		return jsonMapper.writeValueAsString(reponse);
	}
	
// trouver une matiere par mot cle
	
	@GetMapping("/matrech")
	public String find( String mc) throws JsonProcessingException {
		Reponse<List<Matiere>> reponse = null;
		
		try {
			List<Matiere> mats= matiereMetier.rechParMotCle(mc);
			if (!mats.isEmpty()) {
				reponse= new Reponse<List<Matiere>>(0, null, mats);
			}else {
				List<String> messages = new ArrayList<>();
				messages.add("Pas de matières pour  ce mot clé");
				reponse = new Reponse<List<Matiere>>(3, messages, null);
			}
		} catch (Exception e) {
			reponse = new Reponse<List<Matiere>>(1, Static.getErreurforexception(e), null);
		}
		return jsonMapper.writeValueAsString(reponse);
	}

	// toutes les matiières
	
	@GetMapping("/matieres")
	public String findAll() throws JsonProcessingException {
		Reponse<List<Matiere>> reponse = null;
		// liste des matières
		try {
			List<Matiere> matieresTous = matiereMetier.findAll();

			if (!matieresTous.isEmpty()) {
				reponse = new Reponse<List<Matiere>>(0, null, matieresTous);
			} else {
				List<String> messages = new ArrayList<>();
				messages.add("Pas de matières enregistrées à ce jour");
				reponse = new Reponse<List<Matiere>>(3, messages, null);
			}
		} catch (RuntimeException e) {
			reponse = new Reponse<List<Matiere>>(1, Static.getErreurforexception(e), null);
		}
		return jsonMapper.writeValueAsString(reponse);
	}

	// supprimer une collections
	public void spprimer(List<Matiere> entities) {
		matiereMetier.spprimer(entities);
	}

	// supprimer une matiere par son id
	@DeleteMapping("/matieres/{id}")
	public String supprimer(@PathVariable("id") Long id) throws JsonProcessingException {
		// La reponse
				Reponse<Boolean> reponse = null;
				boolean erreur = false;
				Matiere m=null;

				// On recupère la personne
				if (!erreur) {
					Reponse<Matiere> reponseMat = getMatiere(id);
					m=reponseMat.getBody();
					if (reponseMat.getStatus() != 0) {
						reponse = new Reponse<>(reponseMat.getStatus(), reponseMat.getMessages(), false);
						erreur = true;
					}
				}
				if (!erreur) {
					// Suppression
					try {
						List<String> messages = new ArrayList<>();
						messages.add(String.format("[%s] [%s] a été bien supprimer!", m.getId(),m.getLibelle()));
						reponse = new Reponse<>(0, messages, matiereMetier.supprimer(id));
					} catch (RuntimeException e) {
						reponse = new Reponse<>(3, Static.getErreurforexception(e), false);
					}

				}
				// La reponse
				return jsonMapper.writeValueAsString(reponse);
	}

	
	
	public boolean existe(Long id) {
		return matiereMetier.existe(id);
	}

	public Long compter() {
		return matiereMetier.compter();
	}

}
