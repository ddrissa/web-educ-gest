package ci.kossovo.educ.web.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ci.kossovo.educ.web.entity.Personne;
import ci.kossovo.educ.web.exceptions.InvalidEducException;
import ci.kossovo.educ.web.exceptions.InvalidPersonneException;
import ci.kossovo.educ.web.metier.IPersonneMetier;
import ci.kossovo.educ.web.models.Reponse;
import ci.kossovo.educ.web.utilitaires.Static;

@CrossOrigin(origins = {"http://localhost:4200", "http://localhost:4300"})
@RestController
public class PersonneRestService {

	@Autowired
	IPersonneMetier metierPersonne;

	@Autowired
	private ObjectMapper jsonMapper;

	// Methode local
	private Reponse<Personne> getPersonne(Long id) {
		// On recupère la personne
		Personne pers = null;
		try {
			pers = metierPersonne.find(id);
		} catch (RuntimeException e) {
			return new Reponse<>(1, Static.getErreurforexception(e), null);
		}

		// personne existe?
		if (pers == null) {
			List<String> messages = new ArrayList<>();
			messages.add(String.format("La personne [%s] n'existe pas", id));
			return new Reponse<Personne>(2, messages, null);
		}
		
		// OK
		return new Reponse<Personne>(0, null, pers);
	}

	////////////////////////////////////////////////////////////////////////////////////////////

	// Créé une personne
	@PostMapping("/personnes")
	public String creer(@RequestBody Personne p) throws JsonProcessingException {
		Reponse<Personne> reponse;

		try {

			Personne personne = metierPersonne.creer(p);

			List<String> messages = new ArrayList<>();
			messages.add(String.format("[%s] [%s %s] a été creer avec succès!", personne.getId(), personne.getNom(), personne.getPrenom()));
			reponse = new Reponse<Personne>(0, messages, personne);
		} catch (InvalidEducException e) {
			reponse = new Reponse<Personne>(1, Static.getErreurforexception(e), null);
		}
		return jsonMapper.writeValueAsString(reponse);
	}

	// Modifier une personne
	@PutMapping("/personnes")
	public String modifier(@RequestBody Personne p) throws JsonProcessingException {
		Reponse<Personne> reponse;
		Reponse<Personne> reponseModif = getPersonne(p.getId());
		if (reponseModif.getStatus() == 0) {
			try {
				Personne personne=metierPersonne.modifier(p);	
				List<String> messages = new ArrayList<>();
				messages.add(String.format("[%s] [%s %s] a été modifier avec succès!", personne.getId(), personne.getNom(), personne.getPrenom()));
				
				reponse = new Reponse<Personne>(0, messages,personne );
			} catch (InvalidEducException e) {
				reponse = new Reponse<Personne>(1, Static.getErreurforexception(e), null);
			}
		} else {
			reponse = new Reponse<Personne>(reponseModif.getStatus(), reponseModif.getMessages(),
					reponseModif.getBody());
		}

		return jsonMapper.writeValueAsString(reponse);
	}

	// Trouver une personne par son id
	@GetMapping("/personnes/{id}")
	public String findById(@PathVariable("id") Long id) throws JsonProcessingException {
		Reponse<Personne> reponse;
		reponse = getPersonne(id);
		return jsonMapper.writeValueAsString(reponse);
	}

	// La liste des personnes.
	@GetMapping("/typepersonnes/{type}")
	public String findAll(@PathVariable("type") String type) throws JsonProcessingException {
		Reponse<List<Personne>> reponse = null;
		// liste des personnes
		try {
			List<Personne> personnesTous = metierPersonne.personneAll(type);

			if (!personnesTous.isEmpty()) {
				reponse = new Reponse<List<Personne>>(0, null, personnesTous);
			} else {
				List<String> messages = new ArrayList<>();
				messages.add("Pas de personnes enregistrées à ce jour");
				reponse = new Reponse<List<Personne>>(3, messages, null);
			}
		} catch (RuntimeException e) {
			reponse = new Reponse<List<Personne>>(1, Static.getErreurforexception(e), null);
		}
		return jsonMapper.writeValueAsString(reponse);
	}

	public void spprimer(List<Personne> entities) {
		metierPersonne.spprimer(entities);
	}

	@DeleteMapping("/personnes/{id}")
	public String supprimer(@PathVariable("id") Long id) throws JsonProcessingException {
		// La reponse
		Reponse<Boolean> reponse = null;
		boolean erreur = false;
		Personne pp=null;

		// On recupère la personne
		if (!erreur) {
			Reponse<Personne> reponsePers = getPersonne(id);
			pp=reponsePers.getBody();
			if (reponsePers.getStatus() != 0) {
				reponse = new Reponse<>(reponsePers.getStatus(), reponsePers.getMessages(), false);
				erreur = true;
			}
		}
		if (!erreur) {
			// Suppression
			try {
				List<String> messages = new ArrayList<>();
				messages.add(String.format("[%s] [%s %s] a été bien supprimer!", pp.getId(),pp.getNom(),pp.getPrenom()));
				reponse = new Reponse<>(0, messages, metierPersonne.supprimer(id));
			} catch (RuntimeException e) {
				reponse = new Reponse<>(3, Static.getErreurforexception(e), false);
			}

		}
		// La reponse
		return jsonMapper.writeValueAsString(reponse);
	}

	public boolean existe(Long id) {
		return metierPersonne.existe(id);
	}

	public Long compter() {
		return metierPersonne.compter();
	}

	public Personne chercherParMatricule(String matricule) {
		return metierPersonne.chercherParMatricule(matricule);
	}

	public Personne chercherParIdentifiantS(String numCni) {
		return metierPersonne.chercherParIdentifiantS(numCni);
	}

	public List<Personne> chercherUserParMc(String mc) {
		return metierPersonne.chercherUserParMc(mc);
	}

}
