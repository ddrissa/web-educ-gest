package ci.kossovo.educ.web.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import ci.kossovo.educ.web.entity.Matiere;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MattierreTest {
	@Autowired
	TestEntityManager entityManager;
	@Autowired
	MatiereRepository matiereRepository;

	@Test
	public void findAllAndfindOneAndsave() {
		entityManager.persist(new Matiere("Java", "language"));
		entityManager.persist(new Matiere("Ruby", "language"));
		entityManager.persist(new Matiere("Groovy", "language"));
		
		List<Matiere> mat1 = matiereRepository.findAll();
		assertThat(mat1.size()).isEqualTo(3);
		Long id = mat1.get(0).getId();
		Matiere m = matiereRepository.findOne(id);
		assertNotNull(m);
		assertThat(m.getLibelle()).isEqualTo("Java");
		m.setLibelle("Java 8");
		m = matiereRepository.save(m);
		assertThat(m.getLibelle()).isEqualTo("Java 8");
		matiereRepository.delete(id);
		assertThat(matiereRepository.findAll().size()).isEqualTo(2);
		assertNull(matiereRepository.findOne(id));
		assertNotNull(matiereRepository.save(new Matiere("Spring", "Java framework")));
		assertThat(matiereRepository.findAll().size()).isEqualTo(3);
	}
	
	@Test
	public void rechParMotCle(){
		entityManager.persist(new Matiere("Java", "language"));
		entityManager.persist(new Matiere("Java 8", "language"));
		entityManager.persist(new Matiere("Groovy", "language"));
		
		List<Matiere> matRechs= matiereRepository.rechParMotCle("v");
		assertThat(matRechs.size()).isEqualTo(3);
		assertThat(matRechs.get(0).getLibelle()).isEqualTo("Java");
		
		List<Matiere> matRechs0= matiereRepository.rechParMotCle("");
		assertThat(matRechs0.size()).isEqualTo(3);
		
	}

}
