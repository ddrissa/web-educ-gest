package ci.kossovo.educ.web.metier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import ci.kossovo.educ.web.dao.MatiereRepository;
import ci.kossovo.educ.web.entity.Matiere;
import ci.kossovo.educ.web.exceptions.InvalidEducException;


@RunWith(SpringRunner.class)
@SpringBootTest
public class MatiereMetierImplTest {
	@Autowired
	IMatiereMetier matiereMetier;
	@MockBean
	MatiereRepository matiereRepositoryMock;
	
	
	@Test
	public void creerUnePersone() throws InvalidEducException{
		// given
				Matiere m = new Matiere("Java", "Langages");
				Matiere m1 = new Matiere("Ruby", "Langages");
				m1.setId(1L);
				
				given(matiereRepositoryMock.save(m)).willReturn(m1);
				
				
				// when
				Matiere ms = matiereMetier.creer(m);

				// then
				verify(matiereRepositoryMock).save(m);
				assertThat(ms).isEqualTo(m1);
	}
	

	@Test
	public void creerUneMatierSansLibelle() {
		// given

		Matiere m = new Matiere();
		m.setDescription("langage");
		given(matiereRepositoryMock.save(m)).willThrow(new RuntimeException("Le libelle ne peut etre null"));

		// when
		Matiere m1 = null;
		try {
			m1 = matiereMetier.creer(m);
		} catch (InvalidEducException e) {
			e.printStackTrace();
		}

		// then
		verify(matiereRepositoryMock, never()).save(m);
		assertThat(m1).isEqualTo(null);

	}
	
	
	@Test
	public void modifierMatiere() {
		// given
		Matiere mex = new Matiere("java", "langage");
		mex.setId(3L);
		List<Matiere>mats= new ArrayList<>();
		given(matiereRepositoryMock.findByLibelle("Java 8")).willReturn(mats);
		
		mex.setLibelle("Java 8");
		given(matiereRepositoryMock.save(mex)).willReturn(mex);
		
		//when
		Matiere ms=null;
		try {
			 ms=matiereMetier.modifier(mex);
		} catch (InvalidEducException e) {
			e.printStackTrace();
		}
		
		
		verify(matiereRepositoryMock).save(mex);
		
		assertThat(ms.getLibelle()).isEqualTo("Java 8");
		assertThat(ms.getDescription()).isEqualTo(mex.getDescription());
		
	}
	
	
	/*@Test
	public void modifierMatiereParLibelleExistant() {
		// given
		Matiere mex = new Matiere("java", "langage");
		Matiere mex1 = new Matiere("java 8", "langage");
		mex.setId(3L);
		mex1.setId(1L);
		List<Matiere>mats= new ArrayList<>();
		mats.add(mex1);
		given(matiereRepositoryMock.findByLibelle("Java 8")).willReturn(mats);
		
		mex.setLibelle("Java 8");
		given(matiereRepositoryMock.save(mex)).willThrow(new RuntimeException("Ce libelle existe dejà."));
		
		//when
		Matiere ms=null;
		try {
			 ms=matiereMetier.modifier(mex);
		} catch (InvalidEducException e) {
			e.printStackTrace();
		}
		
		
		verify(matiereRepositoryMock, never()).save(mex);
		
		assertThat(ms).isEqualTo(null);
		
		
	}*/

}
