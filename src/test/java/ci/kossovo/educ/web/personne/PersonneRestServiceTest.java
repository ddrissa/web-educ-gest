package ci.kossovo.educ.web.personne;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import ci.kossovo.educ.web.entity.Adresse;
import ci.kossovo.educ.web.entity.Etudiant;
import ci.kossovo.educ.web.entity.Invite;
import ci.kossovo.educ.web.entity.Personne;
import ci.kossovo.educ.web.metier.IPersonneMetier;
import ci.kossovo.educ.web.services.PersonneRestService;

@RunWith(SpringRunner.class)
@WebMvcTest(PersonneRestService.class)
public class PersonneRestServiceTest {
	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private IPersonneMetier personneMetier;

	private ObjectMapper mapper= new ObjectMapper();
	

	// teste findAll()
	@Test
	public void trouverToutesInvites() throws Exception {
	
	//Donnée	
		Personne p1=new Invite("Mr", "Diarra", "Drissa","CNI01");
		p1.setId(1L);
		p1.setType("IN");
		Personne p2=new Invite("Mr", "Traoré", "Abdoulaye","CNI02");
		p2.setId(2l);
		p2.setType("IN");
		List<Personne> personnes= Arrays.asList(p1,p2);
		
		//when
		given(this.personneMetier.personneAll("IN"))
		.willReturn(personnes);
		
		//then
		this.mvc.perform(get("/typepersonnes/IN"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.body.length()", is(2)))
		.andExpect(jsonPath("$.body.[0].numCni", is("CNI01")));
		
	}
	
	
	// teste findAll()
	@Test
	public void trouverToutesEtudianT() throws Exception {
	
	//Donnée	
		Personne p1=new Etudiant("Mr", "Diarra", "Drissa","CNI01","MA01");
		p1.setId(3L);
		p1.setType("ET");
		Personne p2=new Etudiant("Mr", "Traoré", "Abdoulaye","CNI04","MA02");
		p2.setId(4l);
		p2.setType("ET");
		List<Personne> personnes= Arrays.asList(p1,p2);
		
		//when
		given(this.personneMetier.personneAll("ET"))
		.willReturn(personnes);
		
		//then
		this.mvc.perform(get("/typepersonnes/ET"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.body.length()", is(2)))
		.andExpect(jsonPath("$.body.[0].numCni", is("CNI01")));
		
	}
	
	
	//////////////////////////////////////////////////////
	//teste enregistrement personne
	@Test
	public void CreerUnePersonne() throws Exception {

		Personne p=new Invite("Mr", "Diarra", "Drissa","CNI01");
		
		Personne p2 =p;
		p2.setId(1L);
		
		given(this.personneMetier.creer(any(Personne.class))).willReturn(p2);
		
		this.mvc.perform(post("/personnes")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)
		.content(mapper.writeValueAsString(p))
		)
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.status", is(0)))
		.andExpect(jsonPath("$.body.id", is(1)))
		.andExpect(jsonPath("$.body.nom", is("Diarra")));
	}
	
	/////////////////////////////////////////////////
	//teste un modification de personne
	@Test
	public void ModifierUnePersonne() throws Exception {
		
		Personne posModif=new Invite("Mr", "Diarrassouba", "Drissa","CNI01");
		posModif.setId(1L);
		posModif.setType("IN");
		
		
		Personne pexist =new Invite("Mr", "Diarra", "Drissa","CNI01");
		posModif.setId(1L);
		posModif.setType("IN");
		
		Personne prep =new Invite("Mr", "Diarrassouba", "Drissa","CNI01");
		prep.setId(1L);
		prep.setType("IN");
		
		given(this.personneMetier.find(posModif.getId())).willReturn(pexist);
		
		given(this.personneMetier.modifier(any(Personne.class))).willReturn(prep);
		
		this.mvc.perform(put("/personnes")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)
		.content(mapper.writeValueAsString(posModif))
		)
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.status", is(0)))
		.andExpect(jsonPath("$.body.id", is(1)))
		.andExpect(jsonPath("$.body.nom", is("Diarrassouba")));
		verify(this.personneMetier, timeout(1)).find(posModif.getId());
		verify(this.personneMetier, timeout(1)).modifier(posModif);
	}
	
	//////////////////////////////////////////////
	// teste chercher une personne
	@Test
	public void trouverUnePersonne() throws Exception {
	
	//Donnée	
		Personne p1=new Etudiant("Mr", "Traoré", "Abdoulaye","CNI04","MA02");
		p1.setType("IN");
		p1.setId(1L);
		
		
		//when
		given(this.personneMetier.find(p1.getId()))
		.willReturn(p1);
		
		//then
		this.mvc.perform(get("/personnes/{id}", p1.getId()))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.status", is(0)))
		.andExpect(jsonPath("$.body.id", is(1)))
		.andExpect(jsonPath("$.body.nom", is("Traoré")));
		verify(this.personneMetier, timeout(1)).find(p1.getId());
		
		
	}
			
	
	
//////////////////////////////////////////////
// teste supprimer une personne
@Test
public void supprimerUnePersonne() throws Exception {

//Donnée	
Personne p1=new Etudiant("Mr", "Traoré", "Abdoulaye","CNI04","MA02");
p1.setType("IN");
p1.setId(1L);


//when
given(this.personneMetier.find(p1.getId()))
.willReturn(p1);
given(this.personneMetier.supprimer(p1.getId())).willReturn(true);

//then
this.mvc.perform(delete("/personnes/{id}", p1.getId()))
.andExpect(status().isOk())
.andExpect(jsonPath("$.status", is(0)))
.andExpect(jsonPath("$.body", is(true)));
verify(this.personneMetier, timeout(1)).find(p1.getId());
verify(this.personneMetier, timeout(1)).supprimer(p1.getId());

}

}
